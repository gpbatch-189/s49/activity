// console.log("hello")

/*
    fetch() is a method in JS, which allows to send request to an api and process its response. Fetch has 2 arguments, the url to resource/route and optional object which contains additional information about our requests such as method, the body, and the headers of our request.

    fetch () method
    Syntax:
        fetch(url, options)       

    */ 

// Get post data using fetch()

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => console.log(data))

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

// Start of Fetch
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    }) // post lang dito pero hindi pa nas-save sa database/frontend
    //kukunin response
    .then ((response) => response.json())
    .then((data) => {

        console.log(data)
        alert("Successfully Added.")

        // Clear the text elements upon post creation
        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null
    })

// End of Request

})

//Show Posts
const showPosts = (posts) => {

	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Mini Activity: Get or retrieve all the posts. Use showPost function.

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
    showPosts(data)
})

// Edit post -   id = galing sa pinasa ng showPosts, walang .addEventListener dito kasi meron ng onclick sa taas.
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    //Removes the disabled attribute from the button
    document.querySelector('#btn-submit-update').removeAttribute('disabled')

}

//Update Post
// since form sya sa HTML, gagamit tayo ng .addEventListener, galing HTML tong submit btn
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault()

        fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title:document.querySelector('#txt-edit-body').value,
            userId: 1
            }),

            headers: {'Content-type': 'application/json; charset=UTF-8'}

        })
        
        .then((response) => response.json())
        .then((data) => {
            console.log(data)
            alert('Successfully updated')

            document.querySelector('#txt-edit-id').value = null;
            document.querySelector('#txt-edit-title').value = null;
            document.querySelector('#txt-edit-body').value = null;

            document.querySelector('#btn-submit-update').setAttribute('disabled', true)
        })
})

/* 
    Mini-Activity:
        Retrieve a single post, and print it in the console.
*/

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((data) => {console.log(data)})

// Activity

const deletePost = (id) => {

	document.querySelector(`#post-1`).remove();

    }



